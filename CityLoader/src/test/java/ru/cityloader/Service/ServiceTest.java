package ru.cityloader.Service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.cityloader.repository.Repository;
import ru.cityloader.entity.City;
import ru.cityloader.exception.CityNotFoundException;
import ru.cityloader.service.Service;
import ru.cityloader.service.ServiceImpl;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


@ExtendWith(MockitoExtension.class)
public class ServiceTest {

    @Mock
    private Repository repository;
    private final Service service = new ServiceImpl(repository, null);

    @Test
    public void getCityAndIndexWithTheBiggestPopulation() throws IOException, ParseException, CityNotFoundException {
        ArrayList<City> cities = new ArrayList<>();
        cities.add(new City(1, "Москва", "Москва", "Москва", 6000000L, "1115-05-05"));
        cities.add(new City(2, "Москва", "Москва", "Москва", 600000L, "1115-05-05"));
        //when(repository.getData()).thenReturn(cities);

        City city = service.findTheBiggestCity((a, b) -> (int) (a.getPopulation() - b.getPopulation()));

        assertEquals(new City(1, "Москва", "Москва", "Москва", 6000000L, "1115-05-05"), city);
    }

    @Test
    public void testGetCityAndIndexWithTheBiggestPopulation_ButFileEmpty() throws IOException, ParseException, CityNotFoundException {
        //when(repository.getData()).thenReturn(Collections.emptyList());

        CityNotFoundException cityNotFoundException = assertThrows(CityNotFoundException.class, () -> service.findTheBiggestCity((a, b) -> (int) (a.getPopulation() - b.getPopulation())));

        assertEquals("City with max population was not found", cityNotFoundException.getMessage());
    }

    @Test
    public void testGroupByRegion() throws IOException, ParseException {
        ArrayList<City> cities = new ArrayList<>();
        cities.add(new City(1, "Москва", "Москва", "Москва", 6000000L, "1115-05-05"));
        cities.add(new City(2, "Москва", "Москва", "Москва", 600000L, "1115-05-05"));
        cities.add(new City(3, "Москва",    "Калуга", "Москва", 600000L, "1115-05-05"));
        //when(repository.getData()).thenReturn(cities);

        HashMap<String, List<City>> groups = service.groupCityByRegion();

        assertEquals(2,groups.get("Москва").size());
        assertEquals(1,groups.get("Калуга").size());
    }
}
