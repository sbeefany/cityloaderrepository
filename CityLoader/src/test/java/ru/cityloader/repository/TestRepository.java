package ru.cityloader.repository;


import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import ru.cityloader.entity.City;
import ru.cityloader.service.Service;
import ru.cityloader.service.ServiceImpl;
import ru.cityloader.utils.HibernateUtilsFake;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;


public class TestRepository {

    private Repository repository;

    @Test
    public void testGetDataFromEmptyFile() throws IOException, ParseException {
        File file = new File("./testEmptyFile.txt");
        file.createNewFile();
        repository = new FileRepository(file);

        List<City> resultContent = repository.getData();


        assertTrue(resultContent.isEmpty());
    }

    @Test
    public void testGetDataFromFileWithCities() throws IOException, ParseException {
        File file = new File("./testFile.txt");
        file.createNewFile();
        repository = new FileRepository(file);

        List<City> resultContent = repository.getData();

        assertEquals(1, resultContent.size());
        assertFalse(resultContent.isEmpty());
    }

    @Test
    public void testGetDataFromFileWithCitiesCheckData() throws IOException, ParseException {
        File file = new File("./testFile.txt");
        file.createNewFile();
        repository = new FileRepository(file);

        List<City> resultContent = repository.getData();

        assertEquals(new City(1, "Москва", "Москва", "Москва", 6000000L, "1115-05-05"), resultContent.get(0));
    }

    @Test
    public void testGetDataFromDataBase() throws IOException, ParseException {
        File file = new File("./testFile.txt");
        file.createNewFile();
        Repository fileRepository = new FileRepository(file);
        Repository dataBaseRepository = new DataBaseRepository(HibernateUtilsFake.getSessionFactory());

        Service service = new ServiceImpl(dataBaseRepository,fileRepository);

        List<City> result = service.getAllCities();

        assertFalse(result.isEmpty());
    }

    @Test
    public void getDataWithSort() throws ParseException, IOException {
        File file = new File("./testFile2.txt");
        file.createNewFile();
        Repository fileRepository = new FileRepository(file);
        Repository dataBaseRepository = new DataBaseRepository(HibernateUtilsFake.getSessionFactory());
        Service service = new ServiceImpl(dataBaseRepository,fileRepository);

        List<City> data = service.getAllSortedCities(City::compareTo);

        ArrayList<City> expectedList = new ArrayList<>(2);
        expectedList.add(new City(2, "Абакан", "Абакан", "Абакан", 200000L, "1115-05-05"));
        expectedList.add( new City(1, "Москва", "Москва", "Москва", 6000000L, "1115-05-05"));
        assertEquals(expectedList, data);

    }

    @Test
    public void getDataWithSortOnDistrictAndName() throws IOException, ParseException {
        File file = new File("./testFile3.txt");
        file.createNewFile();
        Repository fileRepository = new FileRepository(file);
        Repository dataBaseRepository = new DataBaseRepository(HibernateUtilsFake.getSessionFactory());
        Service service = new ServiceImpl(dataBaseRepository,fileRepository);

        List<City> data = service.getAllSortedCities(City::compareToWithDistrict);

        ArrayList<City> expectedList = new ArrayList<>(2);
        expectedList.add( new City(2, "Абакан", "Абакан", "Абакан", 200000L, "1115-05-05"));
        expectedList.add(new City(1, "Москва", "Москва", "Москва", 6000000L, "1115-05-05"));

        assertEquals(expectedList, data);
    }



}
