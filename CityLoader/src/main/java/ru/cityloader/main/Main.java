 package ru.cityloader.main;


import ru.cityloader.repository.DataBaseRepository;
import ru.cityloader.repository.FileRepository;
import ru.cityloader.repository.Repository;
import ru.cityloader.entity.City;
import ru.cityloader.exception.CityNotFoundException;
import ru.cityloader.service.Service;
import ru.cityloader.service.ServiceImpl;
import ru.cityloader.utils.HibernateUtils;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Main {
    
    private  boolean theEnd = false;

    public static void main(String[] args) {
        new Main().startProgram();
    }

    public void startProgram(){
        while (!theEnd) {
            try {
                Service service = initService();
                showMenu();
                addListener(service);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                System.out.println("Упс, неполадки");
            } catch (UnsupportedOperationException e) {
                System.out.println("Операция недоступна");
            } catch (CityNotFoundException e) {
                System.out.println(e.getMessage());
            } catch (InputMismatchException e) {
                System.out.println("Вводите пожалуйста цифры");
            }
        }
    }

    private void addListener(Service service) throws IOException, ParseException, CityNotFoundException {
        Scanner scan = new Scanner(System.in);
        int choice = scan.nextInt();
        switch (choice) {
            case 1: {
                service.getAllCities().forEach(System.out::println);
                break;
            }
            case 2: {
                service.getAllSortedCities(City::compareTo).forEach(System.out::println);
                break;
            }
            case 3: {
                service.getAllSortedCities(City::compareToWithDistrict).forEach(System.out::println);
                break;
            }
            case 4: {
                City biggestCity = service.findTheBiggestCity(
                        (a, b) -> (int) (a.getPopulation() - b.getPopulation())
                );
                System.out.println("[" + biggestCity.getId() + "] = " + biggestCity.getPopulation());
                break;
            }
            case 5: {
                HashMap<String, List<City>> cityByRegion = service.groupCityByRegion();
                for (String region : cityByRegion.keySet()) {
                    System.out.println(region + "-" + cityByRegion.get(region).size());
                }
                break;
            }
            case 6: {
                theEnd = true;
                System.out.println("Пока-пока");
                break;
            }

            default: {
                throw new UnsupportedOperationException("This operation does not access");
            }

        }
    }

    private void showMenu() {
        System.out.println("CityLoader приветсвует Вас!");
        System.out.println("Меню:");
        System.out.println("1: Список городов.");
        System.out.println("2: Отсортированный список городов.");
        System.out.println("3: Отсортированный список городов по названию и округу.");
        System.out.println("4: Найти самый населенный город.");
        System.out.println("5: Кол-во городов по регионам.");
        System.out.println("6: Выход");

    }

    private Service initService() {
        File file = new File("./Cities.txt");
        Repository repository = new DataBaseRepository(HibernateUtils.getSessionFactory());
        return new ServiceImpl(repository,new FileRepository(file));
    }
}
