package ru.cityloader.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Сущность City
 */
@Data
@Entity(name = "City")
@Table(name = "city")
public class City implements Comparable<City> {

    @Id
    @Column(name = "id")
    @Getter
    @Setter
    private Integer id;
    @Column(name = "name")
    @Getter
    @Setter
    private String name;
    @Column(name = "region")
    @Getter
    @Setter
    private String region;
    @Column(name = "district")
    @Getter
    @Setter
    private String district;
    @Column(name = "population")
    @Getter
    @Setter
    private Long population;
    @Temporal(TemporalType.DATE)
    @Column(name = "foundationdate")
    private Date foundationDate;

    /**
     * Конструктор который принимает строку
     *
     * @param line строковое представление данного класса, которое состоит из следующих полей, разделенных символом «;»:
     *             * Порядковый номер записи справочника;
     *             * Наименование города;
     *             * Регион;
     *             * Федеральный округ;
     *             * Количество жителей;
     *             * Дата основания или первое упоминание;
     * @throws ParseException Выбрасывается если входная строка представлена в другом формате
     */
    public City(String line) throws ParseException {
        String[] strings = line.split(";");
        id = Integer.valueOf(strings[0]);
        name = strings[1];
        region = strings[2];
        district = strings[3];
        population = Long.valueOf(strings[4]);
        foundationDate = createDateFromString(strings[5]);

    }

    /**
     * Конструктор, который инициализирует все поля
     *
     * @param id             - Номер по списку(Уникальный)
     * @param name           - Название города
     * @param region         - Область
     * @param district       - Федеральный округ
     * @param population     - Насиление
     * @param foundationDate - дата основания в формате ("yyyy-MM-dd")
     * @throws ParseException - Выбрасывается, если дата передана в другом формате
     */
    public City(Integer id, String name, String region, String district, Long population, String foundationDate) throws ParseException {
        this.id = id;
        this.name = name;
        this.region = region;
        this.district = district;
        this.population = population;
        this.foundationDate = createDateFromString(foundationDate);
    }

    /**
     * Конструктор без параметров(необходим для JPA)
     */
    public City() {
    }

    private Date createDateFromString(String myDate) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.parse(myDate);
    }

    @Override
    public String toString() {
        return "City{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", region='" + region + '\'' +
                ", district='" + district + '\'' +
                ", population=" + population +
                ", foundationDate=" + foundationDate +
                '}' + '\n' + '\r';
    }

    /**
     * Основной метод для сравнения объектов типа City
     *
     * @param otherCity Объект с которым будет происходить сравнение
     * @return 0 - объекты равны 1 - оБъект больше -1 - меньше
     */
    @Override
    public int compareTo(City otherCity) {
        return this.name.compareTo(otherCity.name);
    }

    /**
     * Альетрнативный метод сравнения
     *
     * @param otherCity Объект с которым будет происходить сравнение
     * @return 0 - объекты равны 1 - оБъект больше -1 - меньше
     */
    public int compareToWithDistrict(City otherCity) {
        int resultDistrictCompare = this.district.compareTo(otherCity.getDistrict());
        if (resultDistrictCompare == 0)
            return this.compareTo(otherCity);
        else
            return resultDistrictCompare;
    }


}
