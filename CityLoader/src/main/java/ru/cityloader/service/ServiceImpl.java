package ru.cityloader.service;

import ru.cityloader.repository.Repository;
import ru.cityloader.entity.City;
import ru.cityloader.exception.CityNotFoundException;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class ServiceImpl implements Service {

    private final Repository repository;
    private final Repository fileRepository;

    /**
     *
     * @param repository основной репозиторий, который использует базу данных
     * @param fileRepository репозиторий, который считывает информацию с файла
     */
    public ServiceImpl(Repository repository, Repository fileRepository){
        this.repository = repository;
        this.fileRepository = fileRepository;

    }

    @Override
    public City findTheBiggestCity(Comparator<City> comparator) throws IOException, ParseException, CityNotFoundException {
        fileRepository.getData().forEach(repository::saveCity);
        return repository.getData().stream().max(comparator).orElseThrow(() -> new CityNotFoundException("City with max population was not found"));

    }

    @Override
    public HashMap<String, List<City>> groupCityByRegion() throws IOException, ParseException {
        fileRepository.getData().forEach(repository::saveCity);
        final HashMap<String,List<City>> groups = new HashMap<>();
        repository.getData().forEach(city -> {
            List<City> list = groups.get(city.getRegion());
            if(list==null)
                list = new ArrayList<>();
            list.add(city);
            groups.put(city.getRegion(),list);
        });
        return groups;
    }

    @Override
    public List<City> getAllCities() throws IOException, ParseException {
        fileRepository.getData().forEach(repository::saveCity);
        return repository.getData();
    }

    @Override
    public List<City> getAllSortedCities(Comparator<City> cityComparator) throws IOException, ParseException {
        fileRepository.getData().forEach(repository::saveCity);
        List<City> data = repository.getData();
        data.sort(cityComparator);
        return data;
    }
}
