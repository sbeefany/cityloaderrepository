package ru.cityloader.service;

import ru.cityloader.entity.City;
import ru.cityloader.exception.CityNotFoundException;

import java.io.IOException;
import java.text.ParseException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public interface Service {

    /**
     * Метод для нахождение самого большого города
     * @param comparator Необходим для того, чтобы понимать по какому принципу идет сравнение
     * @return City.class
     * @throws IOException Если чтото случится с чтением из файла
     * @throws ParseException Если данные файла записаны в неверном формате
     * @throws CityNotFoundException Если максимального значения не было найдено
     */
    City findTheBiggestCity(Comparator<City> comparator) throws IOException, ParseException, CityNotFoundException;

    /**
     * Метод для полуения списка городов по регионам
     * @return Возвращает hashMap, где ключ - Регион, а значение - список городов
     * @throws IOException Если чтото случится с чтением из файла
     * @throws ParseException Если данные файла записаны в неверном формате
     */
    HashMap<String, List<City>> groupCityByRegion() throws IOException, ParseException;

    List<City> getAllCities() throws IOException, ParseException;

    List<City> getAllSortedCities(Comparator<City> cityComparator) throws IOException, ParseException;
}
