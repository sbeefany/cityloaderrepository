package ru.cityloader.exception;

/**
 * Исключение, которое выбрасывается в том случае если не был найден требуемый объект City.class
 */
public class CityNotFoundException extends Exception {
    public CityNotFoundException(String s) {
        super(s);
    }
}
