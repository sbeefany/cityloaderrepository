package ru.cityloader.repository;

import ru.cityloader.entity.City;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;

public class FileRepository implements Repository {

    private final File file;

    public FileRepository(File file) {
        this.file = file;
    }

    @Override
    public List<City> getData() throws IOException, ParseException {
        List<City> cities = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                cities.add(new City(line));
            }
        }
        return cities;
    }

    @Override
    public void saveCity(City city) {

    }


}
