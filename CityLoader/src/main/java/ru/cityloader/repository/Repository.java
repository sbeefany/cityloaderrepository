package ru.cityloader.repository;

import ru.cityloader.entity.City;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

public interface Repository {
    /**
     * Метод для получения данных типа City.class
     * @return   City.class
     * @throws IOException Если чтото случится с чтением из файла
     * @throws ParseException Если данные файла записаны в неверном формате
     */
    List<City> getData() throws IOException, ParseException;

    void saveCity(City city);
}
