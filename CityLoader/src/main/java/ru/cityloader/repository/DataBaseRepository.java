package ru.cityloader.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import ru.cityloader.entity.City;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

public class DataBaseRepository implements Repository {

    private final SessionFactory sessionFactory;
    private Transaction transaction = null;

    public DataBaseRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<City> getData() throws IOException, ParseException {
        return getAllCities();
    }

    @Override
    public void saveCity(City city) {
        saveOrUpdate(city);
    }

    private Session getSessionAndBeginTransaction() {
        Session session = sessionFactory.openSession();

        transaction = session.beginTransaction();
        return session;
    }

    private void saveOrUpdate(City cities) {
        Session session = getSessionAndBeginTransaction();

        closeTransaction(session, transaction);
    }

    private void closeTransaction(Session session, Transaction transaction) {
        transaction.commit();
        session.close();
    }

    private List<City> getAllCities() {
        Session session = getSessionAndBeginTransaction();

        List<City> cities = session.createQuery("from City", City.class).list();

        closeTransaction(session, transaction);

        return cities;
    }
}
